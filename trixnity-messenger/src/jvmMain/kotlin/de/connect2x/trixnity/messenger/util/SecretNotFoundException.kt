package de.connect2x.trixnity.messenger.util

class SecretNotFoundException(message: String?) : RuntimeException(message) {
}