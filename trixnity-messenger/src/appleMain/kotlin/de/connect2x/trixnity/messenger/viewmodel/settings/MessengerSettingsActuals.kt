package de.connect2x.trixnity.messenger.viewmodel.settings

import com.russhwolf.settings.NSUserDefaultsSettings
import com.russhwolf.settings.Settings
import platform.Foundation.NSUserDefaults

actual fun createSettings(): Settings {
    val delegate = NSUserDefaults()
    return NSUserDefaultsSettings(delegate)
}