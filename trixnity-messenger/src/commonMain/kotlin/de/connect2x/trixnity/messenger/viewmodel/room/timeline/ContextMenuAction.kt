package de.connect2x.trixnity.messenger.viewmodel.room.timeline

enum class ContextMenuAction(val value: String) {
    DOWNLOAD("Download"),
    EDIT("Bearbeiten"),
    REDACT("Löschen"),
    ANSWER("Antworten"),
    DEBUG("Debug"),
}