package de.connect2x.trixnity.messenger.viewmodel.files

data class FileTransferProgressElement(val percent: Float, val formattedProgress: String)